package readability;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * A WordCounter that can count syllables and word.
 * @author Arut Thanomwatana
 *
 */
public class WordCounter 
{
	State state; 
	private String URL;
	public int syllables;
	//= "http://cpe.ku.ac.th/~jim/219141/readability/Carroll-simplified.txt";

	/**
	 * countSyllables use for count the syllables of word.
	 * @param word is the word that will be count syllables
	 * @return number of syllables in word 
	 */

	public int countSyllables(String word){
		state = START;
		String cutWord = word.replaceAll("'", "");
		syllables = 0;
		char [] wordCount = cutWord.toCharArray();
		for(char c : wordCount){
			this.state.enterState(c);
		}
		if(state == E_FIRST&&syllables==0)
			syllables++;
		if(state == VOWEL)
			syllables++;
		if(state == DASH)
			return 0;
		if(state == NO_WORD)
			return 0;
		return syllables;
	}
	/**
	 * countWords use for count number of word in the URL.
	 * @return number of word that was counted from URL
	 * @throws IOException if URL is not valid
	 */
	public int countWords() throws IOException{
		int count = 0;

		URL url = new URL(URL);
		InputStream in = url.openStream();
		Scanner scan = new Scanner(in);
		while(scan.hasNext()){
			final String Delim = "[\\s,.\\?!\"():;]+";
			scan.useDelimiter(Delim);
			String check = scan.next();
			if(countSyllables(check)!=0)
				count++;
		}
		return count;

	}
	/**
	 * getSyllableCount use for count every syllable in word in URL.
	 * @return number of syllables in every word that was counted from URL
	 * @throws IOException if URL is not valid
	 */
	public int getSyllableCount() throws IOException{
		int count = 0;
		URL url = new URL(URL);
		InputStream in = url.openStream();
		Scanner scan = new Scanner(in);
		final String Delim = "[\\s,.\\?!\"():;]+";
		scan.useDelimiter(Delim);
		while(scan.hasNext()){
			String check = scan.next();
			count+= this.countSyllables(check);
		}
		return count;
	}
	/**
	 * isVowel use for check wheter char is vowel or not.
	 * @param c is char that will be check wheter it is a vowel or not
	 * @return true if c is a vowel.
	 */
	public boolean isVowel(char c)
	{
		if(c=='a'||c=='e'||c=='i'||c=='o'||c=='u'||c=='A'||c=='E'||c=='O'||c=='U'||c=='I'){
			return true;
		}
		return false;
	}

	public int countSentence() throws IOException
	{
		int count = 0;
		URL url = new URL(URL);
		InputStream in = url.openStream();
		Scanner scan = new Scanner(in);
		while(scan.hasNext())
		{
			String check = scan.next();
			count+=getSentenceCount(check);
		}

		return count;
	}

	/**
	 * Use to check whether the word end the sentence or not.
	 * @param word is the word that will be check whether if it end the sentence or not
	 * @return number of sentence that will be counted.
	 */
	public int getSentenceCount(String word)
	{
		int count =0;

		if(countSyllables(word)!=0){
			if(word.endsWith(".")){
				count++;
			}
			else if(word.endsWith("!")){
				count++;
			}
			else if(word.endsWith("?")){
				count++;
			}
			else if(word.endsWith(";")){
				count++;
			}
			else if(word.endsWith("\"")){
				count++;
			}
			else if(word.endsWith(":")){
				count++;
			}
		}
		return count;


	}
	
	/**
	 * Use for set URL.
	 * @param URL is the new URL that will be set
	 */
	public void setURL(String URL){
		this.URL = URL;
	}

	/**
	 *Use for set the State.
	 *@param newState is the state that will be set
	 */
	public void setState(State newState){
		this.state = newState;
	}

	State E_FIRST = new State(){

		public void enterState(char c) {
			
			if(isVowel(c)) setState(VOWEL);
			else if(Character.isLetter(c)) {
				syllables++;
				setState(CONSONANT);
			}
			else if(c=='-'){
				setState(DASH);
			}
			else setState(NO_WORD);

		}
		
	};

	State VOWEL = new State(){
		public void enterState(char c) {
			if(isVowel(c)) setState(VOWEL);
			else if(Character.isLetter(c)){
				syllables++;
				setState(CONSONANT);
			}
			else if(c=='-'){
				syllables++;
				setState(DASH);
			}
			else setState(NO_WORD);

		}
	};

	State CONSONANT = new State(){
		public void enterState(char c){
			if(c=='e'||c=='E') setState(E_FIRST);
			else if(isVowel(c)||c=='y'||c=='Y') setState(VOWEL);
			else if(Character.isLetter(c)) setState(CONSONANT);
			else if(c=='-') setState(DASH);
			else setState(NO_WORD);
		}
	};
	
	State DASH = new State(){
		public void enterState(char c){
			if(c=='e'||c=='E') setState(E_FIRST);
			else if(isVowel(c)) setState(VOWEL);
			else if(Character.isLetter(c)) setState(CONSONANT);
			else setState(NO_WORD);
		}
	};
	
	State NO_WORD = new State(){
		
		public void enterState(char c){
			
		}
	};
	
	State START = new State(){
		
		public void enterState(char c){
			if(c=='e'||c=='E') setState(E_FIRST);
			else if(isVowel(c)) setState(VOWEL);
			else if(Character.isLetter(c)) setState(CONSONANT);
			else setState(NO_WORD);
		}
	};
}
