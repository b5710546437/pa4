package readability;

import java.io.IOException;
/**
 * 
 * @author Arut Thanomwatana
 *
 */
public class ReadabilityConsole 
{
	/** WordCounter that used to count the text */
	private WordCounter counter;
	
	/**
	 * Initialize the Readability.
	 * @param counter is the word counter that use to count the text
	 */
	public ReadabilityConsole(WordCounter counter)
	{
		this.counter = counter;
		
	}
	
	/**
	 * Use for get the amount of syllable from wordcounter.
	 * @return amount of syllable in the text
	 * @throws IOException if URL in method cannot read.
	 */
	public int getSyllable() throws IOException{
		return this.counter.getSyllableCount();
	}
	
	/**
	 * Use for get the amount of word from wordcounter.
	 * @return amount of word in the text
	 * @throws IOException if URL in method cannot read.
	 */
	public int getWord() throws IOException{
		return this.counter.countWords();
	}
	
	/**
	 * Use for get the amount fo word from wordcounter.
	 * @return amount of sentence in the text
	 * @throws IOException if URL in method cannot read.
	 */
	public int getSentence() throws IOException{
		return this.counter.countSentence();
	}
	
	/**
	 * Use for get the Flesch index number.
	 * @return Flesch index number
	 * @throws IOException if URL in method cannot read.
	 */
	public double getFleschIndex() throws IOException{
		
		return 206.835 - 84.6*(counter.getSyllableCount()*1.0/counter.countWords()*1.0)-1.015*(counter.countWords()*1.0/counter.countSentence()*1.0);
	}
	
	/**
	 * Use for set new URL.
	 * @param URL is the new URL that will be set.
	 */
	public void setURL(String URL){
		this.counter.setURL(URL);
	}
	
	/**
	 * Use for get the result of the Flesch index.
	 * @return String that define the result
	 * @throws IOException if URL in method cannot read.
	 */
	public String getFleschIndexToString() throws IOException
	{
		if(this.getFleschIndex()>100)
			return "4th grade student (elementary school)" ;
		else if(this.getFleschIndex()>90)
			return "5th grade student";
		else if(this.getFleschIndex()>80)
			return "6th grade student";
		else if(this.getFleschIndex()>70)
			return "7th grade student";
		else if(this.getFleschIndex()>65)
			return "8th grade student";
		else if(this.getFleschIndex()>60)
			return "9th grade student";
		else if(this.getFleschIndex()>50)
			return "High school student";
		else if(this.getFleschIndex()>30)
			return "College student";
		else if(this.getFleschIndex()>0)
			return "College graduate";
		else
			return "Advanced degree graduate";
	}
	
	
	

}
