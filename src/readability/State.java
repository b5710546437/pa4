package readability;
/**
 * State enum use for keep every state of WordCounter.
 * @author Arut Thanomwatana
 *
 */
public interface State {
	public void enterState(char c);
}
