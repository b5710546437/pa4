package readability.Readability;
import java.io.IOException;
import java.net.URL;

import readability.ReadabilityConsole;
import readability.ReadabilityGUI;
import readability.WordCounter;

/**
 * Main use for test the WordCounter.
 * @author Arut Thanomwatana
 *
 */
public class Main 
{
	/**
	 * Main is use for test word counter and time it.
	 * @param args not used 
	 * @throws IOException if URL is not valid
	 */
	public static void main(String [] args) throws IOException{
		WordCounter wordCount = new WordCounter();
		ReadabilityConsole read = new ReadabilityConsole(wordCount);
		if(args.length>0){
			wordCount.setURL(args[0]);
			System.out.printf("Filename:\t\t%s\nNumber of Syllables:\t\t%d\nNumber of Words:\t\t%d\nNumber of Sentences:\t\t%d\nFlesch Index:\t\t%.1f\nReadability:\t\t%s"
					, args[0],wordCount.getSyllableCount(),wordCount.countWords(),wordCount.countSentence(),read.getFleschIndex(),read.getFleschIndexToString());
		
		}
		else{
		ReadabilityGUI gui = new ReadabilityGUI(read);
		}
	}

}
