package readability;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ReadabilityGUI extends JFrame
{
	private ReadabilityConsole counter;
	private JLabel label1;
	private JTextField URLText;
	private JFileChooser browse;
	private JButton browseBtn;
	private JButton countBtn;
	private JButton clearBtn;
	private JTextArea resultArea;

	public ReadabilityGUI(ReadabilityConsole counter)
	{
		this.counter = counter;
		this.setTitle("Flesch Readability Index");
		this.initComponent();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}

	public void initComponent()
	{
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
		JPanel upperPane = new JPanel();
		upperPane.setLayout(new FlowLayout());
		label1 = new JLabel("File or URL name: ");

		URLText = new JTextField(20);

		browseBtn = new JButton("Browse...");
		browseBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				browse = new JFileChooser();
				browse.showOpenDialog(panel);
				URLText.setText("file:"+browse.getSelectedFile().getPath());
			}

		});

		countBtn = new JButton("Count");
		countBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				try {
					String result="";
					counter.setURL(URLText.getText());
					result+="File name:     " + browse.getSelectedFile().getName()+"\n";
					result+="Number of Words:      "+counter.getWord()+ "\n";
					result+="Number of Syllables:      " + counter.getSyllable()+ "\n";
					result+="Number of Sentences:     "+counter.getSentence()+ "\n";
					result+=String.format("flesch Index:     %.1f\n",counter.getFleschIndex());
					result+="Readability:     "+counter.getFleschIndexToString();
					resultArea.setText(result);
				} catch (IOException e1) {
					
				}
				
			}
		});
		
		clearBtn = new JButton("Clear");
		clearBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				URLText.setText("");
				resultArea.setText("");
			}
		});
		
		upperPane.add(label1);
		upperPane.add(URLText);
		upperPane.add(browseBtn);
		upperPane.add(countBtn);
		upperPane.add(clearBtn);
		
		JPanel lowerPane = new JPanel();
		
		resultArea = new JTextArea(8,50);
		
		lowerPane.add(resultArea);
		
		panel.add(upperPane);
		panel.add(lowerPane);
		super.add(panel);
		this.pack();
	}

}
